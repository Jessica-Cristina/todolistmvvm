import 'package:flutter/material.dart';

import 'app/src/models/task_model.dart';
import 'app/src/view_models/task_view_model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late final TaskViewModel viewModel = TaskViewModel();
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  var flag = false;

  void addTask(TaskModel task) {
    viewModel.addTask(task);
    clear();
  }

  void clear() {
    titleController.clear();
    descriptionController.clear();
  }

  void dialogWidget(TaskModel task) {
    titleController.text = task.title;
    descriptionController.text = task.description;
    var popupTitle = "";
    flag ? popupTitle = "Edite a tarefa" : popupTitle = "Adicione a tarefa";
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
                title: Text(popupTitle),
                content: Column(children: [
                  TextField(
                    decoration: InputDecoration(labelText: 'Titulo'),
                    controller: titleController,
                  ),
                  TextField(
                    decoration: InputDecoration(labelText: 'Descrição'),
                    controller: descriptionController,
                  )
                ]),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        clear();
                        Navigator.pop(context, 'Cancel');
                      },
                      child: Text("Cancelar")),
                  TextButton(
                      onPressed: () {
                        flag
                            ? viewModel.editTask(task, titleController.text,
                                descriptionController.text)
                            : addTask(TaskModel(descriptionController.text,
                                titleController.text));
                        flag = false;
                        Navigator.pop(context, 'Ok');
                      },
                      child: Text("Confirmar"))
                ])).then((value) => clear());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              //deveria usar containers para margens?
              height: 20,
            ),
            const Text(
              "To do List",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            Expanded(
                child: AnimatedBuilder(
              animation: viewModel,
              child: Text('Lista vaiza'),
              builder: (BuildContext context, Widget? child) {
                if (viewModel.tasks.isEmpty) {
                  return child!; //aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                }

                return ListView.builder(
                    itemCount: viewModel.tasks.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Expanded(
                          child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {
                                flag = true;
                                dialogWidget(viewModel.tasks[index]);
                              },
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      viewModel.tasks[index].title,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                    Text(
                                      viewModel.tasks[index].description,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                  ]),
                            ),
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 5,
                                  ),
                                  ElevatedButton(
                                      child: Icon(Icons.remove),
                                      onPressed: () {
                                        viewModel
                                            .removeTask(viewModel.tasks[index]);
                                      }),
                                ])
                          ],
                        ),
                      ));
                    });
              },
            ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Increment',
          child: const Icon(Icons.add),
          onPressed: () {
            flag = false;
            final task =
                TaskModel(descriptionController.text, titleController.text);
            dialogWidget(task);
          }),
    );
  }
}
