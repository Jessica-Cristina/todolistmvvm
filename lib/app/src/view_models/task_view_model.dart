import 'package:exemplomvvm/app/src/models/task_model.dart';
import 'package:flutter/material.dart';

class TaskViewModel extends ChangeNotifier {
  final List<TaskModel> tasks = <TaskModel>[];

  void addTask(TaskModel task) {
    tasks.add(task);
    notifyListeners();
  }

  void removeTask(TaskModel task) {
    tasks.remove(task);
    notifyListeners();
  }

  void editTask(TaskModel task, String title, String description) {
    task.title = title;
    task.description = description;
    notifyListeners();
  }
}
